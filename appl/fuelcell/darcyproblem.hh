// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_DARCY_SUBPROBLEM_ONEPTHREEC_HH
#define DUMUX_DARCY_SUBPROBLEM_ONEPTHREEC_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/flux/maxwellstefanslaw.hh>

#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/h2on2o2_extend.hh>
//#include <dumux/material/fluidsystems/h2on2o2_extend_diffusioncoefficients.hh>
#include <dumux/material/fluidsystems/h2oheo2.hh>
//#include <dumux/material/chemistry/electrochemistry/electrochemistryni.hh>

#include <dumux/material/electrochemistryni.hh>
#include "spatialparams.hh"

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>
#include <dumux/io/gnuplotinterface.hh>

namespace Dumux
{
template <class TypeTag>
class DarcySubProblem;

namespace Properties
{
NEW_TYPE_TAG(DarcyOnePThreeCTypeTag, INHERITS_FROM(CCTpfaModel, TwoPNCNI));

// Set the problem property
SET_TYPE_PROP(DarcyOnePThreeCTypeTag, Problem, Dumux::DarcySubProblem<TypeTag>);


// Set fluid configuration
SET_PROP(DarcyOnePThreeCTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::H2ON2O2<Scalar>;
};

// Use moles
SET_BOOL_PROP(DarcyOnePThreeCTypeTag, UseMoles, true);

// Do not replace one equation with a total mass balance
SET_INT_PROP(DarcyOnePThreeCTypeTag, ReplaceCompEqIdx, 5);

//! Use a model with constant tortuosity for the effective diffusivity
SET_TYPE_PROP(DarcyOnePThreeCTypeTag, EffectiveDiffusivityModel,
              DiffusivityConstantTortuosity<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Set the grid type
SET_TYPE_PROP(DarcyOnePThreeCTypeTag, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

//Set the diffusion type
SET_TYPE_PROP(DarcyOnePThreeCTypeTag, MolecularDiffusionType, DIFFUSIONTYPE);

// Set the spatial paramaters type
SET_TYPE_PROP(DarcyOnePThreeCTypeTag, SpatialParams, TwoPTwoCSpatialParams<TypeTag>);

SET_TYPE_PROP(DarcyOnePThreeCTypeTag, ThermalConductivityModel, ThermalConductivitySomerton<typename GET_PROP_TYPE(TypeTag, Scalar)>);
//set the primary variable for the second phase, which means the gas phase
SET_BOOL_PROP(DarcyOnePThreeCTypeTag, SetMoleFractionsForFirstPhase, false);

SET_PROP(DarcyOnePThreeCTypeTag, Formulation)
{ static constexpr auto value = TwoPFormulation::p1s0; };

}

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    enum {
        // grid and world dimension
        //dim = GridView::dimension,                            //6.
        dimworld = GridView::dimensionworld,                    //.7

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;
    static constexpr int dim = GridView::dimension;                   //5.
    static constexpr int dimWorld = GridView::dimensionworld;         //5. 
    
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using ElectroChemistry = typename Dumux::ElectroChemistry<Scalar, Indices,  FluidSystem, FVGridGeometry, ElectroChemistryModel::Ochs>;


public:
    DarcySubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Darcy"), eps_(1e-9), couplingManager_(couplingManager)
    {
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        initialSaturationWater_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.SaturationWater");
        initialMoleFractionO2_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialMoleFractionO2");
        FluidSystem::init();
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const // define output
    { return true; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}


    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time)
    {
       // compute the mass in the entire domain
        Scalar massWater;
        Scalar massO2;
        Scalar evaporation = 0;
        Scalar N2;                                    //N2
        Scalar O2; //O2
        Scalar reactionSourceH2O;
        Scalar reactionSourceO2;
        
        // bulk elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const auto& globalPos = scv.dofPosition();
                for(int phaseIdx = 0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
                {
                    massWater += volVars.massFraction(phaseIdx, FluidSystem::H2OIdx)*volVars.density(phaseIdx)
                    * scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                    massO2 += volVars.massFraction(phaseIdx, FluidSystem::O2Idx)*volVars.density(phaseIdx)
                    * scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                }
                if(inReactionLayer_(globalPos))
                {
                    //reactionSource Output
                    PrimaryVariables source;
                    auto i = ElectroChemistry::calculateCurrentDensity(volVars);
                    ElectroChemistry::reactionSource(source, i);

                    reactionSourceH2O += source[Indices::conti0EqIdx + FluidSystem::H2OIdx]*scv.volume()* volVars.extrusionFactor();
                    reactionSourceO2 += source[Indices::conti0EqIdx + FluidSystem::O2Idx]*scv.volume()* volVars.extrusionFactor();  
                }

            }
            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    continue;

                // NOTE: binding the coupling context is necessary
                couplingManager_->bindCouplingContext(CouplingManager::darcyIdx, element);
                const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, DiffusionCoefficientAveragingType::harmonic);

                evaporation += flux[0] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                N2 += flux[1] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();                 //N2
                O2 += flux[2] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();                 //O2

            }
        }
        // convert to kg/s if using mole fractions
        evaporation = evaporation * FluidSystem::molarMass(FluidSystem::H2OIdx);                                    //N2
        N2 = N2 * FluidSystem::molarMass(1);
        O2 = O2 * FluidSystem::molarMass(2);                                                                        //O2
        std::cout<<std::setprecision(12)<<"mass of water is: " << massWater << std::endl;
        std::cout<<"mass o2 in pm "<<massO2<<std::endl;
        std::cout<<"evaporation from pm "<<evaporation<<std::endl;
        std::cout<<"N2 from pm "<<N2<<std::endl; //air
        std::cout<<"O2 from pm "<<O2<<std::endl;   //He


         //do a gnuplot
        x_.push_back(time); // in seconds
        y_.push_back(evaporation);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0,std::max(time, 300.0));
        gnuplot_.setYRange(0, 5e-5);
        gnuplot_.setXlabel("time [s]");
        gnuplot_.setYlabel("kg/s");
        gnuplot_.addDataSetToPlot(x_, y_, "evaporation");
        gnuplot_.plot("evaporation");

        //do a gnuplot
        y2_.push_back(massWater);

        gnuplot2_.resetPlot();
        gnuplot2_.setXRange(0,std::max(time, 300.0));
        gnuplot2_.setYRange(0, 10);
        gnuplot2_.setXlabel("time [s]");
        gnuplot2_.setYlabel("kg");
        gnuplot2_.addDataSetToPlot(x_, y2_, "water mass");
        gnuplot2_.plot("watermass");

        //do a gnuplot
        y3_.push_back(N2);

        gnuplot3_.resetPlot();
        gnuplot3_.setXRange(0,std::max(time, 300.0));
        gnuplot3_.setYRange(-3e-7, 3e-7);
        gnuplot3_.setXlabel("time [s]");
        gnuplot3_.setYlabel("kg/s");
        gnuplot3_.addDataSetToPlot(x_, y3_, "N2");
        gnuplot3_.plot("N2"); 

        //do a gnuplot
        y4_.push_back(O2);

        gnuplot4_.resetPlot();
        gnuplot4_.setXRange(0,std::max(time, 300.0));
        gnuplot4_.setYRange(-1.5e-5, 1.5e-5);
        gnuplot4_.setXlabel("time [s]");
        gnuplot4_.setYlabel("kg/s");
        gnuplot4_.addDataSetToPlot(x_, y4_, "O2");
        gnuplot4_.plot("O2");
        
        //do a gnuplot
        y5_.push_back(massO2);

        gnuplot5_.resetPlot();
        gnuplot5_.setXRange(0,std::max(time, 300.0));
        gnuplot5_.setYRange(0, 10);
        gnuplot5_.setXlabel("time [s]");
        gnuplot5_.setYlabel("kg");
        gnuplot5_.addDataSetToPlot(x_, y5_, "O2 mass");
        gnuplot5_.plot("O2mass");
        
        //do a gnuplot
        y6_.push_back(reactionSourceH2O);

        gnuplot6_.resetPlot();
        gnuplot6_.setXRange(0,std::max(time, 300.0));
        gnuplot6_.setYRange(0, 10);
        gnuplot6_.setXlabel("time [s]");
        gnuplot6_.setYlabel("mol/s");
        gnuplot6_.addDataSetToPlot(x_, y6_, "reaction rate");
        gnuplot6_.plot("reactionSourceH2O");
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolumeFace &scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values.setAllCouplingNeumann();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if (couplingManager().isCoupledEntity(couplingManager().darcyIdx, scvf))
        {
            const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, DiffusionCoefficientAveragingType::harmonic);

            for(int i = 0; i< massFlux.size(); ++i)
                values[i] = massFlux[i];

            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scvf, DiffusionCoefficientAveragingType::harmonic);
        }
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary subcontrolvolumeface
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(globalPos);
        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The subcontrolvolume
     */
    template<class ElementVolumeVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scv.dofPosition();
        //reaction sources from electro chemistry
        if(inReactionLayer_(globalPos))
        {
             const auto& volVars = elemVolVars[scv];
             auto currentDensity = ElectroChemistry::calculateCurrentDensity(volVars);
             ElectroChemistry::reactionSource(values, currentDensity);
        }

        return values;
    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values.setState(Indices::bothPhases);
        values[pressureIdx] = pressure_;
        values[Indices::switchIdx] = initialSaturationWater_;
        values[conti0EqIdx + 2] = initialMoleFractionO2_;
        values[Indices::temperatureIdx] = 343.15;                     //343.15
        return values;
    }

    // \}

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    template<class VTKWriter>
    void addVtkFields(VTKWriter& vtk)
    {
        const auto& gridView = this->fvGridGeometry().gridView();
        currentDensity_.resize(gridView.size(0));
        reactionSourceH2O_.resize(gridView.size(0));
        reactionSourceO2_.resize(gridView.size(0));
        Kxx_.resize(gridView.size(0)); // 1.
        Kyy_.resize(gridView.size(0)); // 1.
        
        
        
        vtk.addField(currentDensity_, "currentDensity [A/cm^2]");
        vtk.addField(reactionSourceH2O_, "reactionSourceH2O [mol/(sm^2)]");
        vtk.addField(reactionSourceO2_, "reactionSourceO2 [mol/(sm^2)]");
        vtk.addField(Kxx_, "Kxx"); // 2.
        vtk.addField(Kyy_, "Kyy"); // 2.
    }

    void updateVtkFields(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto& globalPos = scv.dofPosition();
                const auto dofIdxGlobal = scv.dofIndex();

                if(inReactionLayer_(globalPos))
                {
                    //reactionSource Output
                    PrimaryVariables source;
                    auto i = ElectroChemistry::calculateCurrentDensity(volVars);
                    ElectroChemistry::reactionSource(source, i);

                    reactionSourceH2O_[dofIdxGlobal] = source[Indices::conti0EqIdx + FluidSystem::H2OIdx];
                    reactionSourceO2_[dofIdxGlobal] = source[Indices::conti0EqIdx + FluidSystem::O2Idx];      //O2

                    //Current Output in A/cm^2
                    currentDensity_[dofIdxGlobal] = i/10000;
                }
                else
                {
                    reactionSourceH2O_[dofIdxGlobal] = 0.0;
                    reactionSourceO2_[dofIdxGlobal] = 0.0;
                    currentDensity_[dofIdxGlobal] = 0.0;
                }
                Kxx_[dofIdxGlobal] = volVars.permeability()[0][0]; //3.
                Kyy_[dofIdxGlobal] = volVars.permeability()[1][1]; //3.
            }
        }
    }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }

    bool inReactionLayer_(const GlobalPosition& globalPos) const
    //{ return globalPos[1] < 0.2*(this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1]) + eps_; }
       //{ const auto y = globalPos[1];
        { return (globalPos[1] < 0.00005 + eps_);}

    Scalar eps_;
    Scalar pressure_;
    Scalar initialSaturationWater_;
    Scalar initialMoleFractionO2_;


    std::shared_ptr<CouplingManager> couplingManager_;

    std::vector<double> x_;
    std::vector<double> y_;
    std::vector<double> y2_;
    std::vector<double> y3_;
    std::vector<double> y4_;
    std::vector<double> y5_;
    std::vector<double> y6_;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    Dumux::GnuplotInterface<double> gnuplot3_;
    Dumux::GnuplotInterface<double> gnuplot4_;
    Dumux::GnuplotInterface<double> gnuplot5_;
    Dumux::GnuplotInterface<double> gnuplot6_;


    std::vector<double> currentDensity_;
    std::vector<double> reactionSourceH2O_;
    std::vector<double> reactionSourceO2_;
    std::vector<double> Kxx_; //4.
    std::vector<double> Kyy_; //4.

};
} //end namespace

#endif //DUMUX_DARCY_SUBPROBLEM_HH
