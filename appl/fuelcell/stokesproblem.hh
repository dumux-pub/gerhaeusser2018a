// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief A simple Navier-Stokes test problem for the staggered grid (Navier-)Stokes model.
 */
#ifndef DUMUX_STOKES_SUBPROBLEM_ONEPTHREEC_HH
#define DUMUX_STOKES_SUBPROBLEM_ONEPTHREEC_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2on2o2_extend.hh>
//#include <dumux/material/fluidsystems/h2on2o2_extend_diffusioncoefficients.hh>
#include <dumux/material/fluidsystems/h2oheo2.hh>
#include <dumux/flux/maxwellstefanslaw.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>
#include <dumux/io/gnuplotinterface.hh> //
namespace Dumux
{
template <class TypeTag>
class StokesSubProblem;

namespace Properties
{
NEW_TYPE_TAG(StokesOnePThreeCTypeTag, INHERITS_FROM(StaggeredFreeFlowModel, NavierStokesNCNI));

// Set fluid configuration
SET_PROP(StokesOnePThreeCTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using H2ON2O2 = FluidSystems::H2ON2O2<Scalar>;       //1
    static constexpr auto phaseIdx = H2ON2O2::gasPhaseIdx; // simulate the gas phase //2
public:
    using type = FluidSystems::OnePAdapter<H2ON2O2, phaseIdx>; //3
};

// Set the grid type
SET_TYPE_PROP(StokesOnePThreeCTypeTag, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Set the problem property
SET_TYPE_PROP(StokesOnePThreeCTypeTag, Problem, Dumux::StokesSubProblem<TypeTag> );

SET_BOOL_PROP(StokesOnePThreeCTypeTag, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(StokesOnePThreeCTypeTag, EnableGridFluxVariablesCache, true);
SET_BOOL_PROP(StokesOnePThreeCTypeTag, EnableGridVolumeVariablesCache, false); //false

// Use moles
SET_BOOL_PROP(StokesOnePThreeCTypeTag, UseMoles, true);

// Set the grid type
SET_TYPE_PROP(StokesOnePThreeCTypeTag, MolecularDiffusionType, DIFFUSIONTYPE);

// Do not replace one equation with a total mass balance
SET_INT_PROP(StokesOnePThreeCTypeTag, ReplaceCompEqIdx, 5);

}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the 1pnc (Navier-) Stokes problem.
 *
 * Horizontal flow from left to right with a parabolic velocity profile.
 */
template <class TypeTag>
class StokesSubProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector); //4.
    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

    //static constexpr int dimWorld = GridView::dimensionworld; //2.
    //using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>; //3.
    
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

public:
    StokesSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Stokes"), eps_(1e-6), injectionState_(false), couplingManager_(couplingManager)
    {
        inletVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Velocity");
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        //inletMoleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletMoleFraction"); //
    }

   /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    { return false; }

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    //Scalar temperature() const
    //{ return 273.15 + 10; } // 10°C

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }
    // \}

   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        const auto& globalPos = scvf.dofPosition();

        values.setNeumann(Indices::energyEqIdx);

        if(onLeftBoundary_(globalPos))
        {
            values.setDirichlet(Indices::energyEqIdx);
            values.setDirichlet(Indices::conti0EqIdx + 2);
            values.setDirichlet(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        else if(onRightBoundary_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
            values.setOutflow(Indices::conti0EqIdx + 1);
            values.setOutflow(Indices::conti0EqIdx + 2);
            values.setOutflow(Indices::energyEqIdx);

        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setNeumann(Indices::conti0EqIdx);
            values.setNeumann(Indices::conti0EqIdx + 1);
            values.setNeumann(Indices::conti0EqIdx + 2);
        }

        if(couplingManager().isCoupledEntity(couplingManager().stokesIdx, scvf))
        {
            values.setNeumann(Indices::conti0EqIdx);               
            values.setNeumann(Indices::conti0EqIdx+1);
            values.setNeumann(Indices::conti0EqIdx+2);
            values.setNeumann(Indices::momentumYBalanceIdx);
            values.setBJS(Indices::velocityXIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element
     * \param scvf The sub control volume face
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(globalPos);

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     * \param scvf The boundary sub control volume face
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {  
        NumEqVector values(0.0);

        if(couplingManager().isCoupledEntity(couplingManager().stokesIdx, scvf))
        {
            values[Indices::momentumYBalanceIdx] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);

            const auto tmp = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, DiffusionCoefficientAveragingType::harmonic);
            values[Indices::conti0EqIdx] = tmp[0];
            values[Indices::conti0EqIdx + 1] = tmp[1];
            values[Indices::conti0EqIdx + 2] = tmp[2];

            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, DiffusionCoefficientAveragingType::harmonic);
            
        }
        return values;
        
    }
        
    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time)
  {
        Scalar H2O;
        Scalar N2;                                    //N2
        Scalar O2; //O2
        
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

        
         auto elemFaceVars = localView(gridVariables.curGridFaceVars());
        elemFaceVars.bindElement(element, fvGeometry, curSol);
        
        
        for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const auto& globalPos = scv.dofPosition();
                //for(int phaseIdx = 0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
                //{
                  //  massH2O += volVars.massFraction(phaseIdx, FluidSystem::H2OIdx)*volVars.density(phaseIdx)
                    //* scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                    //massN2 += volVars.massFraction(phaseIdx, FluidSystem::O2Idx)*volVars.density(phaseIdx)
                    //* scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                    //massO2 += volVars.massFraction(phaseIdx, FluidSystem::O2Idx)*volVars.density(phaseIdx)
                 //* scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                //}
        
        
            }
            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
                    continue;

                // NOTE: binding the coupling context is necessary
                couplingManager_->bindCouplingContext(CouplingManager::stokesIdx, element);
                
                const auto tmp = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, DiffusionCoefficientAveragingType::harmonic);
                H2O += tmp[0] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor(); //
                N2 += tmp[1] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();            //     
                O2 += tmp[2] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();  
                
            }
        }
        
        std::cout<<"evaporation rate stokes "<<H2O<<std::endl;
        
        H2O = H2O * FluidSystem::molarMass(0);                                   
        N2 = N2 * FluidSystem::molarMass(1);
        O2 = O2 * FluidSystem::molarMass(2);                                    
        
        
        x_.push_back(time); // in seconds
        y_.push_back(H2O);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0,std::max(time, 300.0));
        gnuplot_.setYRange(0, 5e-5);
        gnuplot_.setXlabel("time [s]");
        gnuplot_.setYlabel("kg/s");
        gnuplot_.addDataSetToPlot(x_, y_, "H2O");
        gnuplot_.plot("H2O");

        //do a gnuplot
        y2_.push_back(O2);

        gnuplot2_.resetPlot();
        gnuplot2_.setXRange(0,std::max(time, 300.0));
        gnuplot2_.setYRange(0, 10);
        gnuplot2_.setXlabel("time [s]");
        gnuplot2_.setYlabel("kg");
        gnuplot2_.addDataSetToPlot(x_, y2_, "O2stokes");
        gnuplot2_.plot("O2stokes");

        //do a gnuplot
        y3_.push_back(N2);

        gnuplot3_.resetPlot();
        gnuplot3_.setXRange(0,std::max(time, 300.0));
        gnuplot3_.setYRange(-3e-7, 3e-7);
        gnuplot3_.setXlabel("time [s]");
        gnuplot3_.setYlabel("kg/s");
        gnuplot3_.addDataSetToPlot(x_, y3_, "N2stokes");
        gnuplot3_.plot("N2stokes"); 

    }

    // \}

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = pressure_;
        values[Indices::velocityXIdx] = inletVelocity_ * (globalPos[1] - this->fvGridGeometry().bBoxMin()[1])
                                              * (this->fvGridGeometry().bBoxMax()[1] - globalPos[1])
                                              / (0.25 * (this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1])
                                              * (this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1]));
        values[Indices::conti0EqIdx+1] = 0.02; //h2o 0.02
        values[Indices::conti0EqIdx+2] = 0.21; //02  0.21

        values[Indices::temperatureIdx] = 343.15; //343.15
        return values;
    }

    /*!
     * \brief Returns the intrinsic permeability of required as input parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
    {
        return couplingManager().couplingData().darcyPermeability(element, scvf);
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    {
        return 1.0;
    }


    void setInjectionState(const bool yesNo)
    {
        injectionState_ = yesNo;
    }

    bool isInjectionPeriod() const
    {
        return injectionState_;
    }

    // \}

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }

    //DimWorldMatrix permeability_; //4.
    Scalar eps_;
    Scalar inletVelocity_;
    Scalar pressure_;
    //Scalar inletMoleFraction_;
    bool injectionState_;

    std::shared_ptr<CouplingManager> couplingManager_;
    std::vector<double> x_;  //
    std::vector<double> y_;  //
    std::vector<double> y2_; //
    std::vector<double> y3_; //
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    Dumux::GnuplotInterface<double> gnuplot3_;
};
} //end namespace

#endif // DUMUX_STOKES_SUBPROBLEM_HH
