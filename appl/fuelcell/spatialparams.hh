// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OnePTests
 * \brief The spatial parameters class for the test problem using the 1p cc model
 */
#ifndef DUMUX_CONSERVATION_SPATIAL_PARAMS_HH
#define DUMUX_CONSERVATION_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/regularizedacosta.hh> //7.
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/material/fluidmatrixinteractions/thermalconductivityconstant.hh> //8.
#include <dumux/material/chemistry/electrochemistry/electrochemistryni.hh> //
namespace Dumux
{

/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p cc model
 */
template<class TypeTag>
class TwoPTwoCSpatialParams
: public FVSpatialParams<typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                         typename GET_PROP_TYPE(TypeTag, Scalar),
                         TwoPTwoCSpatialParams<TypeTag>>
{   
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, TwoPTwoCSpatialParams<TypeTag>>;
    static constexpr int dimWorld = GridView::dimensionworld; //1.

    
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using EffectiveLaw = RegularizedAcosta<Scalar>; //9.
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>; //2.

public:
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;
    using PermeabilityType = DimWorldMatrix; //3. //Scalar

    TwoPTwoCSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry), permeability_(0) //6.
    {
        permeability_[0][0] = 7.5e-12;  //4.
        permeability_[1][1] = 3.9e-11;

        
        //permeability_ = getParam<Scalar>("SpatialParams.Permeability");
        porosity_ = getParam<Scalar>("SpatialParams.Porosity");
        tortuosity_ = getParam<Scalar>("SpatialParams.Tortuosity");
        tortuositycatalystlayer_ = getParam<Scalar>("SpatialParams.TortuosityCatalystLayer");
        //catalystlayerlength_ = getParam<Scalar>("SpatialParams.CatalystLayerLength");
        porositycatalystlayer_ = getParam<Scalar>("SpatialParams.PorosityCatalystLayer");
        alphaBJ_ = getParam<Scalar>("SpatialParams.AlphaBeaverJoseph");

        // residual saturations
        params_.setSwr(getParam<Scalar>("SpatialParams.Swr"));
        params_.setSnr(getParam<Scalar>("SpatialParams.Snr"));
        // parameters for the vanGenuchten law
        //params_.setVgAlpha(getParam<Scalar>("SpatialParams.VgAlpha")); //10.
        //params_.setVgn(getParam<Scalar>("SpatialParams.VgN")); //11.
        
        //parameters for acosta
        params_.setAcA(getParam<Scalar>("SpatialParams.acA")); //12.
        params_.setAcB(getParam<Scalar>("SpatialParams.acB")); //13.
        params_.setAcC(getParam<Scalar>("SpatialParams.acC")); //14.
        params_.setAcD(getParam<Scalar>("SpatialParams.acD")); //15.
        params_.setAcE(getParam<Scalar>("SpatialParams.acE")); //16.
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *
     * \param globalPos The global position
     * \return the intrinsic permeability
     */
     DimWorldMatrix permeabilityAtPos(const GlobalPosition& globalPos) const //5. Scalar
    { return permeability_; }  //permeability_

    /*! \brief Define the porosity in [-].
     *
     * \param globalPos The global position
     */Scalar tortuosityAtPos(const GlobalPosition& globalPos) const
    { if(inReactionLayer_(globalPos))    //
        return tortuositycatalystlayer_;
    return tortuosity_; }
    
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { if(inReactionLayer_(globalPos))    //
        return porositycatalystlayer_;
    return porosity_; }

    /*! \brief Define the Beavers-Joseph coefficient in [-].
     *
     * \param globalPos The global position
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition& globalPos) const
    { return alphaBJ_; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *        In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return the material parameters object
     */
    template<class ElementSolutionVector>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolutionVector& elemSol) const
    { return params_; }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The global position
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; } //1.

private:
    bool inReactionLayer_(const GlobalPosition& globalPos) const
    //{ return globalPos[1] < 0.2*(this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1]) + eps_; } //
    //{ const auto y = globalPos[1];
        { return (globalPos[1] < 0.00005 + eps_);}
    DimWorldMatrix permeability_; //5.
    //Scalar permeability_; //Scalar
    Scalar porosity_;
    Scalar tortuosity_;
    Scalar porositycatalystlayer_;
    Scalar tortuositycatalystlayer_;
    //Scalar catalystlayerlength_;
    Scalar alphaBJ_;
    MaterialLawParams params_;
    static constexpr Scalar eps_ = 1.0e-9;
};

} // end namespace

#endif
