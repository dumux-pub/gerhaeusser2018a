add_input_file_links()

dune_add_test(NAME test_stokes1p3cdarcy2p3chorizontal_maxwellstefan
              SOURCES test_stokes1p3cdarcy2p3chorizontal.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMPILE_DEFINITIONS DIFFUSIONTYPE=MaxwellStefansLaw<TypeTag>)

dune_add_test(NAME test_stokes1p3cdarcy2p3chorizontal_fickslaw
              SOURCES test_stokes1p3cdarcy2p3chorizontal.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMPILE_DEFINITIONS DIFFUSIONTYPE=FicksLaw<TypeTag>)
