// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   Implementation of the capillary pressure and
 *          water phase saturation according to Acosta.
 */
#ifndef REGULARIZED_ACOSTA_HH
#define REGULARIZED_ACOSTA_HH

#include "acosta.hh"
#include "regularizedacostaparams.hh"

//#include <algorithm>
//#include <cmath>
//#include <cassert>

#include <dumux/common/spline.hh>

namespace Dumux
{
/*!\ingroup fluidmatrixinteractionslaws
 *
 * \brief Implementation of the regularized  Acosta
 *        capillary pressure / relative permeability  <-> saturation relation.
 *        This class bundles the "raw" curves as
 *        static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 *        In order to avoid very steep gradients the marginal values are "regularized".
 *        This means that in stead of following the curve of the material law in these regions, some linear approximation is used.
 *        Doing this is not worse than following the material law. E.g. for very low wetting phase values the material
 *        laws predict infinite values for \f$p_c\f$ which is completely unphysical. In case of very high wetting phase
 *        saturations the difference between regularized and "pure" material law is not big.
 *
 *        Regularizing has the additional benefit of being numerically friendly: Newton's method does not like infinite gradients.
 *
 *        The implementation is accomplished as follows:
 *        - check whether we are in the range of regularization
 *         - yes: use the regularization
 *         - no: forward to the standard material law.
 *
 *         For an example figure of the regularization: RegularizedVanGenuchten
 *
 * \see Acosta
 */

/*!
 *
 * For general info: EffToAbsLaw
 *
 * \see AcostaParams
 */

template <class ScalarT, class ParamsT = RegularizedAcostaParams<ScalarT> >
class RegularizedAcosta
{
    typedef Dumux::AcostaPcSw<ScalarT, ParamsT> Acosta;

public:
    typedef ParamsT     Params;
    typedef typename    Params::Scalar Scalar;

    /*!
     * \brief A regularized Acosta capillary pressure-saturation
     *        curve.
     *
     * regularized part:
     *    - low saturation:  extend the \f$p_c(S_w)\f$ curve with the slope at the regularization point (i.e. no kink).
     *    - high saturation: connect the high regularization point with \f$ \overline S_w =1\f$ by a straight line (yes, there is a kink :-( ).
     *
     * For the non-regularized part:
     *
     * \copydetails Acosta::pc()
     */
    static Scalar pc(const Params &params, Scalar swe)
    {
//      return 0.0;
        const Scalar sThres = params.thresholdSw();

        // make sure that the capilary pressure observes a
        // derivative != 0 for 'illegal' saturations. This is
        // required for example by newton solvers (if the
        // derivative is calculated numerically) in order to get the
        // saturation moving to the right direction if it
        // temporarily is in an 'illegal' range.

        //imbibition
        if (params.acE() == 0.0)
        {
            if (swe < sThres) {
                Scalar m = Acosta::dpc_dsw(params, sThres);
                Scalar pcsweLow = Acosta::pc(params, sThres);
                return pcsweLow + m*( swe - sThres);
            }
            else if (swe >= (1.0 - sThres)) {
                Scalar m = Acosta::dpc_dsw(params, (1.0 - sThres));
                Scalar pcsweHigh = Acosta::pc(params, (1.0 - sThres));
                return pcsweHigh + m*(swe - (1.0 - sThres));
            }
        }
        //drainage
        else
        {
            if (swe <= sThres) {
                Scalar m = Acosta::dpc_dsw(params, sThres);
                Scalar pcsweLow = Acosta::pc(params, sThres);
                return pcsweLow + m*(swe - sThres);
            }
            else if (swe >= (1.0 - sThres)) {
                Scalar m = Acosta::dpc_dsw(params, (1.0 - sThres));
                Scalar pcsweHigh = Acosta::pc(params, (1.0 - sThres));
                return pcsweHigh + m*(swe - (1.0 - sThres));
            }
        }
        // if the effective saturation is in an 'reasonable'
        // range, we use the real Acosta law...
        return Acosta::pc(params, swe);
    }

    /*!
         * \brief   A regularized Acosta saturation-capillary pressure curve.
         *
         * regularized part:
         *    - low saturation:  extend the \f$p_c(S_w)\f$ curve with the slope at the regularization point (i.e. no kink).
         *    - high saturation: connect the high regularization point with \f$ \overline S_w =1\f$ by a straight line (yes, there is a kink :-( ).
         *
         *  The according quantities are obtained by exploiting theorem of intersecting lines.
         *
         * For the non-regularized part:
         *
         * \copydetails Acosta::sw()
         */
        static Scalar sw(const Params &params, Scalar pc)
        {
            DUNE_THROW(Dune::NotImplemented, "Acosta::sw(params, pc)");
        }

        /*!
             * \brief A regularized version of the partial derivative
             *        of the \f$p_c(\overline S_w)\f$ w.r.t. effective saturation
             *        according to Acosta.
             *
             * regularized part:
             *    - low saturation:  use the slope of the regularization point (i.e. no kink).
             *    - high saturation: connect the high regularization point with \f$ \overline S_w =1\f$ by a straight line and use that slope (yes, there is a kink :-( ).
             *
             * For the non-regularized part:
             *
             * \copydetails Acosta::dpc_dsw()
             */
            static Scalar dpc_dsw(const Params &params, Scalar swe)
            {
                const Scalar sThres = params.thresholdSw();

                // derivative of the regualarization
                if (swe < 0) {
                    // calculate the slope of the straight line used in pc()
                    Scalar m = Acosta::dpc_dsw(params, 0);
                    return m;
                }
                else if (swe >= (1.0 - sThres)) {
                    // calculate the slope of the straight line used in pc()
                    Scalar m = Acosta::dpc_dsw(params, (1.0 - sThres));
                    return m;
                }

                return Acosta::dpc_dsw(params, swe);
            }

    /*!
     * \brief The partial derivative of the effective
     *        saturation to the capillary pressure according to Acosta.
     *
     *        function does not exist yet!
     *
     * \param pc        Capillary pressure \f$p_C\f$
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dsw_dpc(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::dsw_dpc(params, pc)");
    }

    /*!
         * \brief   Regularized version of the  relative permeability
         *          for the wetting phase of
         *          the medium implied by the Acosta
         *          parameterization.
         *
         *  regularized part:
         *    - below \f$ \overline S_w =0\f$:                  set relative permeability to zero
         *    - above \f$ \overline S_w =1\f$:                  set relative permeability to one
         *    - between \f$ 0.95 \leq \overline S_w \leq 1\f$:  use a spline as interpolation
         *
         *  For not-regularized part:
            \copydetails Acosta::krw()
         */
        static Scalar krw(const Params &params, Scalar swe)
        {
            if (swe <= 0.0)
                return 0.0;
            else if (swe >= 1.0)
                return 1.0;

            return Acosta::krw(params, swe);
        }


    /*!
     * \brief The derivative of the relative permeability for the
     *        wetting phase in regard to the wetting saturation of the
     *        medium implied by the Acosta parameterization.
     *
     * \param swe       The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dkrw_dsw(const Params &params, Scalar swe)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::dkrw_dsw(params, swe)");
    };

    /*!
     * \brief   Regularized version of the  relative permeability
     *          for the non-wetting phase of
     *          the medium implied by the Acosta
     *          parameterization.
     *
     * regularized part:
     *    - below \f$ \overline S_w =0\f$:                  set relative permeability to zero
     *    - above \f$ \overline S_w =1\f$:                  set relative permeability to one
     *    - for \f$ 0 \leq \overline S_w \leq 0.05 \f$:     use a spline as interpolation
     *
         \copydetails Acosta::krn()
     *
     */
    static Scalar krn(const Params &params, Scalar swe)
    {
        if (swe >= 1.0)
            return 0.0;
        else if (swe <= 0.0)
            return 1.0;

        return Acosta::krn(params, swe);
    }

    /*!
     * \brief The derivative of the relative permeability for the
     *        non-wetting phase in regard to the wetting saturation of
     *        the medium as implied by the Acosta
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dkrn_dsw(const Params &params, Scalar swe)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::dkrn_dsw(params, swe)");
    };
 };
}
#endif
