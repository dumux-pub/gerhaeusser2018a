SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

S. Gerhaeusser<br>
Vergleich von Mehrkomponentendiffusionsansätzen im Kontext der Anwendung auf Brennstoffzellen
<br>
Bachelor's Thesis, 2019<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir Gerhaeusser2018a && cd Gerhaeusser2018a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Gerhaeusser2018a.git
```

After that, execute the file [installGerhaeusser2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Gerhaeusser2018a/raw/master/installGerhaeusser2018a.sh).
```
cd Gerhaeusser2018a
./installGerhaeusser2018a.sh
```

This should automatically download all necessary modules and check out the correct versions. Furthermore, a patch to `dumux` is applied and dunecontrol is run.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installGerhaeusser2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Gerhaeusser2018a/raw/master/installGerhaeusser2018a.sh).


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Gerhaeusser2018a
cd Gerhaeusser2018a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/gerhaeusser2018a/-/raw/master/docker_gerhaeusser2018a.sh
```

Open the Docker Container
```bash
bash docker_gerhaeusser2018a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd Gerhaeusser2018a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake/appl/fuelcell folder. They can be executed with an input file e.g., by running

```bash
cd appl/fuelcell
./test_stokes1p3cdarcy2p3chorizontal_fickslaw test_stokes1p3cdarcy2p3chorizontal.input
```
