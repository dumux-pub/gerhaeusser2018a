#!/bin/sh

### Create a folder for the DUNE and DuMuX modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Gerhaeusser2018a.git Gerhaeusser2018a

### Go to specific branches
cd dune-common && git checkout releases/2.6 && cd ..
cd dune-geometry && git checkout releases/2.6 && cd ..
cd dune-grid && git checkout releases/2.6 && cd ..
cd dune-istl && git checkout releases/2.6 && cd ..
cd dune-localfunctions && git checkout releases/2.6 && cd ..
cd dumux && git checkout master && cd ..
cd Gerhaeusser2018a && git checkout master && cd ..

### Go to specific commits and apply patch
cd dumux
git checkout bde9f86af79ea1f35a9493743985b0cad0b3f13a
git am ../Gerhaeusser2018a/dumux_patch.patch
cd ..


### Run dunecontrol
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
